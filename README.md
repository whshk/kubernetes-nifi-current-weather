# Current Weather
![alt text](nifi.png "Nifi")

## Create Namespace
```
$ kubectl create -f namespace-current-weather.yaml
```

## Create ConfigMap
```
$ kubectl create configmap current-weather-config --from-file=current_weather_etl.groovy --namespace current-weather
```

## Deploy Nifi
```
$ kubectl create -f nifi-statefulset-aks.yaml --namespace current-weather
```

```
kubectl create -f https://raw.githubusercontent.com/whs-dot-hk/kubernetes-nifi-refined/master/nifi-service.yaml --namespace current-weather
kubectl create -f https://raw.githubusercontent.com/whs-dot-hk/kubernetes-nifi-refined/master/frontend.yaml --namespace current-weather
```

## Config Nifi
First, upload and add template

### Config StandardSSLContextService
![alt text](standardsslcontextservice-config.png "StandardSSLContextService Config")

## Add DistriburedMapCacheServer
![alt text](add-distributedmapcacheserver.png "Add DistriburedMapCacheServer")

Enable all controller services

and

Update PutEmail and PutAzureBlobStorage
